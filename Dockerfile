FROM openjdk:11.0.3-jdk-slim-stretch
EXPOSE 8080
ADD build/libs/jenkins-gradle-0.0.1-SNAPSHOT.war jenkins-gradle
ENTRYPOINT ["java", "-jar", "/jenkins-gradle", "--spring.profiles.active=dev"]