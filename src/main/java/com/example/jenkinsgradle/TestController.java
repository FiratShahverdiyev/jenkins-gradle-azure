package com.example.jenkinsgradle;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("get")
    public String get() {
        return "hello";
    }

    @GetMapping
    public String devops() {
        return "Ejdaha devopsumuz hazir";
    }
    @GetMapping("test")
    public String test() {
        return "Buna baxanlarin Allah komeyi olsun. Amin!";
    }
    @GetMapping("test2")
    public String test2() {
        return "Buna baxanlarin Allah komeyi 2 qat olsun. Amin!";
    }
}
